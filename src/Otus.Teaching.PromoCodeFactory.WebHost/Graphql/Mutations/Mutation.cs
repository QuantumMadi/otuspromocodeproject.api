using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotChocolate;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Graphql.Mutations
{
    public class Mutation
    {
        public async Task<string> CreateCustomersAsync([Service]IRepository<Customer> customerRepository,[Service]IRepository<Preference> preferenceRepository,
         string firstName,
         string lastName,
         string email,
         List<Guid> preferncesIds)
        {
            var request = new CreateOrEditCustomerRequest{
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                PreferenceIds = preferncesIds,
            };

            var preferences = await preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds);

            Customer customer = CustomerMapper.MapFromModel(request, preferences);
            
            await customerRepository.AddAsync(customer);

            return $"The customer {customer.Id} has been created";
        }

        public async Task<string> EditCustomer(
        [Service]IRepository<Customer> customerRepository, 
        [Service]IRepository<Preference> preferenceRepository,
         Guid customerId,
         string firstName,
         string lastName,
         string email,
         List<Guid> preferncesIds){
             var customer = await customerRepository.GetByIdAsync(customerId);
            
            if (customer == null)
                return "NotFound";
            
            var preferences = await preferenceRepository.GetRangeByIdsAsync(preferncesIds);
            
            var request = new CreateOrEditCustomerRequest{
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                PreferenceIds = preferncesIds
            };

            CustomerMapper.MapFromModel(request, preferences, customer);

            await customerRepository.UpdateAsync(customer);

            return "NoContent";
         }
         
         public async Task<string> DeleteCustomer([Service]IRepository<Customer> customersRepo, Guid id){

            var customer = await customersRepo.GetByIdAsync(id);
            
            if (customer == null)
                return "NotFound";

            await customersRepo.DeleteAsync(customer);

            return "Customer successfully deleted";
         }
    
    }
}