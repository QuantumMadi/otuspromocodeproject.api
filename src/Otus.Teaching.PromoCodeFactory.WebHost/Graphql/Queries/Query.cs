using System.Collections.Generic;
using HotChocolate;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Graphql.Queries
{
    public class Query
    {
        public async Task<List<Customer>> GetCustomers([Service] IRepository<Customer> customerRepositry)
        {
            return (await customerRepositry.GetAllAsync()).ToList();
        }

        public async Task<Customer> GetCustomerById([Service] IRepository<Customer> customerRepository, Guid id)
        {
            return await customerRepository.GetByIdAsync(id);
        }

        public async Task<List<Preference>> GetPrefernces([Service] IRepository<Preference> preferenceRepository){
            return (await preferenceRepository.GetAllAsync()).ToList();
        }
    }
}